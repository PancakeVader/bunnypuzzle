﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileRotator : MonoBehaviour, IClickable
{
    public void ClickedAction()
    {
        TileObject tileObj = GetComponentInChildren<TileObject>();
		tileObj.RotateTile();
    }
}