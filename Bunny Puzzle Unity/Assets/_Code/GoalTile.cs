﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalTile : MonoBehaviour
{
    private void Start()
    {
        int x = Board.Instance.Width - 1;
        int y = Board.Instance.Height;

        transform.position = new Vector3(x, y);
    }
}