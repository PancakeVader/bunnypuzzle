﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    void Update()
    {
		if(Input.GetMouseButtonDown(0))
		{
			Vector3 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			RaycastHit2D hit = Physics2D.Raycast(clickPos, Vector2.zero);

			if(hit)
			{
                IClickable click = hit.transform.GetComponent<IClickable>();
				if(click != null)
				{
					click.ClickedAction();
				}
			}

			// User did not click on UI element so we need to figure out if they clicked
			// next to the board
			else
			{
				bool leftSide = clickPos.x < 0;
				if(leftSide)
					Board.Instance.PushTile((int)clickPos.y, Vector3.right);
				else
                    Board.Instance.PushTile((int)clickPos.y, Vector3.left);
			}
		}
    }
}