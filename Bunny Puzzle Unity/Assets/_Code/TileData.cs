﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TileData : ScriptableObject
{    
    [SerializeField] private Sprite _tileSprite;

    [HideInInspector]
    [SerializeField] private Directions _directions;

    private int _rotations;

    public Directions Directions
    {
        get { return _directions;  }
        set { _directions = value; }
    }

    public Sprite TileSprite
    {
        get { return _tileSprite;  }
        //set { _tileSprite = value; }
    }

    public void RotateClockwise()
    {
        // Mask both shifts with 0x0F (1111 binary) since we only care about the first 4 bits
        int a = (int)_directions << 1 & 0x0F;
        int b = (int)_directions >> (4 - 1) & 0x0F;
        _directions = (Directions)(a | b);
    }
}