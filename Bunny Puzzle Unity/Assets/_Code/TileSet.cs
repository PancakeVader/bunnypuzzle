﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TileSet : ScriptableObject
{
    public TileData[] Tiles;

    public TileData GetRandomTile()
    {
        return Tiles[Random.Range(0, Tiles.Length)];
    }
}
