﻿using UnityEngine;

public class HeartAnimController : MonoBehaviour
{
    private Animator _animator;

    void Start()
    {
        _animator = GetComponent<Animator>();

        // Subscribe to the event
        Board.OnGoalReached += StartAnimation;
    }

    private void OnDisable()
    {
        Board.OnGoalReached -= StartAnimation;
    }

    private void StartAnimation()
    {
        GetComponent<SpriteRenderer>().enabled = true;
        _animator.enabled = true;
        _animator.Play("HeartGrowth");
    }
}