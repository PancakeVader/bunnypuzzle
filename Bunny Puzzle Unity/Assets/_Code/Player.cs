﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float _moveSpeed = 10;
    private Vector2Int _position = new Vector2Int();

    private bool _canMove = true;

    private void Start()
    {
        Board.OnGoalReached += GoalReached;
    }

    private void OnDisable()
    {
        Board.OnGoalReached -= GoalReached;
    }

    void Update()
    {
        _position.x = (int)transform.position.x;
        _position.y = (int)transform.position.y;

        if(_canMove)
            HandleInput();
    }

    private void HandleInput()
    {
        // Right
        if (Input.GetKeyDown(KeyCode.D))
        {
            if (Board.Instance.CheckPathBetweenTiles(_position.x, _position.y, _position.x + 1, _position.y))
            {
                StartCoroutine(MovePlayer(1, 0, _moveSpeed));
            }

        }
        // Left
        else if (Input.GetKeyDown(KeyCode.A))
        {
            if (Board.Instance.CheckPathBetweenTiles(_position.x, _position.y, _position.x - 1, _position.y))
            {
                StartCoroutine(MovePlayer(-1, 0, _moveSpeed));
            }
        }
        // Up
        else if (Input.GetKeyDown(KeyCode.W))
        {
            if (Board.Instance.CheckPathBetweenTiles(_position.x, _position.y, _position.x, _position.y + 1))
            {
                StartCoroutine(MovePlayer(0, 1, _moveSpeed));
            }
        }
        // Down
        else if (Input.GetKeyDown(KeyCode.S))
        {
            if (Board.Instance.CheckPathBetweenTiles(_position.x, _position.y, _position.x, _position.y - 1))
            {
                StartCoroutine(MovePlayer(0, -1, _moveSpeed));
            }
        }
    }

    private IEnumerator MovePlayer(float x, float y, float speed)
    {
        _canMove = false;
        Vector2 targetPos = (Vector2)transform.position + new Vector2(x, y);

        while(Vector2.Distance(transform.position, targetPos) > 0.05f)
        {
            // move the player
            transform.position = Vector2.Lerp(transform.position, targetPos, speed * Time.deltaTime);
            yield return null;
        }
        transform.position = targetPos;
        _canMove = true;
    }

    private void GoalReached()
    {
        _canMove = false;
        StartCoroutine(MovePlayer(0, 0.9f, 2));
        Debug.Log("Goal Reached!");
    }
}