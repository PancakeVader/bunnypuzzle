﻿public interface IClickable
{
	void ClickedAction();
}