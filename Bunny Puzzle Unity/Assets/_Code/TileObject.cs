﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileObject : MonoBehaviour
{
    [SerializeField] private TileData _tileData;
    private SpriteRenderer _spriteRenderer;

    public TileData TileData
    {
        get { return _tileData; }
        set { _tileData = value; }
    }

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        // Create an instance of the ScriptableObject in order
        // to have unique tiles
        _tileData = Instantiate(_tileData);
        
        _spriteRenderer.sprite = _tileData.TileSprite;
    }

    public void RotateTile()
    {
        transform.Rotate(0, 0, -90);
        _tileData.RotateClockwise();
    }
}