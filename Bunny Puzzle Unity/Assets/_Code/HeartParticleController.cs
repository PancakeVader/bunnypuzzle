﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartParticleController : MonoBehaviour
{
    private ParticleSystem _particles;

    void Start()
    {
        _particles = GetComponent<ParticleSystem>();

        // Subscribe to the event
        Board.OnGoalReached += StartAnimation;
    }

    private void OnDisable()
    {
        Board.OnGoalReached -= StartAnimation;
    }

    private void StartAnimation()
    {
        _particles.Play();
    }
}