﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    public static Board Instance = null;

    [SerializeField] private int _width;
    [SerializeField] private int _height;
    [SerializeField] private float _tileSpeed = 10f;
    [SerializeField] private TileSet _tileSet;
    [SerializeField] private Row _rowTemplate;
    [SerializeField] private TileObject _extraTile;
    [SerializeField] private Row[] _rows;

    private Transform _extraTileParent;

    // DELEGATES
    public delegate void GoalReached();
    public static event GoalReached OnGoalReached;

    public int Width
    {
        get { return _width; }
    }

    public int Height
    {
        get { return _height; }
    }

    private void Awake()
    {

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        InitializeBoard();
        _extraTileParent = _extraTile.transform.parent;
    }

    private void InitializeBoard()
    {
        _rows = new Row[_height];

        for (int x = 0; x < _width; x++)
        {
            _rows[x] = Instantiate(_rowTemplate, new Vector3(0, x, 0), Quaternion.identity);
            _rows[x].SetupRow(x, _width);
        }
    }

    public void PushTile(int rowIndex, Vector3 direction)
    {
        // TODO: Add check to see if we are outside the bounds
        _extraTile.transform.parent = _rows[rowIndex].transform.parent;
        
        if(direction == Vector3.right)
        {
            _extraTile = _rows[rowIndex].PushTilesRight(_extraTile, _tileSpeed);
        }
        else if(direction == Vector3.left)
        {
            _extraTile = _rows[rowIndex].PushTilesLeft(_extraTile, _tileSpeed);
        }

        _extraTile.transform.parent = _extraTileParent.transform;
        StartCoroutine(MoveExtraTile());
    }

    private IEnumerator MoveExtraTile()
    {
        while(Vector3.Distance(_extraTile.transform.position, _extraTileParent.transform.position) > 0.05f)
        {
            _extraTile.transform.position = Vector3.Lerp(_extraTile.transform.position,
                _extraTileParent.transform.position,
                _tileSpeed * Time.deltaTime);
            yield return null;
        }
    }

    public bool CheckPathBetweenTiles(int startX, int startY, int targetX, int targetY)
    {
        // If we are going outside of the board return false
        // Check if we are reaching the goal and invoke the event
        if (targetX < 0 || targetY < 0 || targetX > _width - 1 || targetY > _height - 1)
        {
            if (startX == _width - 1 && startY == _height - 1)
            {
                OnGoalReached();
            }

            return false;
        }

        TileData startTile = _rows[startY].row[startX].TileData;
        TileData endTile = _rows[targetY].row[targetX].TileData;

        // Check that start has a way west and target has a way east
        if (startX > targetX)
            return (startTile.Directions & Directions.West) == Directions.West &&
                (endTile.Directions & Directions.East) == Directions.East;

        // Check that start has a way east and target has a way west
        else if (startX < targetX)
            return (startTile.Directions & Directions.East) == Directions.East &&
                (endTile.Directions & Directions.West) == Directions.West;

        // Check that start has a way north and target has a way south
        else if (startY < targetY)
        {
            return (startTile.Directions & Directions.North) == Directions.North &&
            (endTile.Directions & Directions.South) == Directions.South;
        }

        // Check that start has a way south and target has a way north
        else if (startY > targetY)
            return (startTile.Directions & Directions.South) == Directions.South &&
                (endTile.Directions & Directions.North) == Directions.North;

        return false;
    }

}