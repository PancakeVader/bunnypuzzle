﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class Row : MonoBehaviour
{
    [SerializeField] private TileSet _tileSet;
    [SerializeField] private TileObject[] _row;

    public TileObject[] row
    {
        get { return _row; }
    }

    public void SetupRow(int rowIndex, int rowLength)
    {
        _row = new TileObject[rowLength];

        GameObject currentTile;
        TileObject currentTileObj;

        for (int i = 0; i < _row.Length; i++)
        {
            currentTile = new GameObject();
            currentTile.transform.position = new Vector3(i, rowIndex);
            currentTile.name = "Tile_" + i + "_" + rowIndex;
            currentTile.AddComponent<SpriteRenderer>();
            currentTile.transform.parent = transform;

            currentTileObj = currentTile.AddComponent<TileObject>();
            currentTileObj.TileData = _tileSet.GetRandomTile();

            _row[i] = currentTileObj;
            // TODO: Add random rotation to the tiles   
        }
    }

    public TileObject PushTilesRight(TileObject pushedTile, float tileMoveSpeed)
    {
        // 1. Take a reference to the last tile on the right
        // 2. Shift all the tiles to the right by 1
        // 3. Add pushedTile to the first entry off the road

        TileObject lastTile = _row.Last();

        for (int i = _row.Length - 1; i > 0; i--)
        {
            _row[i] = _row[i - 1];
        }

        _row[0] = pushedTile;

        StartCoroutine(MoveNewTile(tileMoveSpeed, true));

        return lastTile;
    }

    public TileObject PushTilesLeft(TileObject pushedTile, float tileMoveSpeed)
    {
        TileObject lastTile = _row.First();

        for (int i = 0; i < _row.Length - 1; i++)
        {
            _row[i] = _row[i + 1];
        }

        _row[_row.Length - 1] = pushedTile;

        StartCoroutine(MoveNewTile(tileMoveSpeed, false));

        return lastTile;
    }

    private IEnumerator MoveNewTile(float tileMoveSpeed, bool isLeftSide)
    {
        if (isLeftSide)
        {
            while (Vector3.Distance(_row[0].transform.position, 
            transform.position + Vector3.left) > 0.1f)
            {
                _row[0].transform.position = Vector3.Lerp(_row[0].transform.position,
                    transform.position + Vector3.left,
                    tileMoveSpeed * Time.deltaTime);
                yield return null;
            }

            _row[0].transform.position = transform.position + Vector3.left;
            yield return StartCoroutine(MoveRow(Vector3.right, tileMoveSpeed));
        }
        else
        {
            while (Vector3.Distance(_row.Last().transform.position,
            (transform.position + (Vector3.right * _row.Length))) > 0.1f)
            {
                _row.Last().transform.position = Vector3.Lerp(_row.Last().transform.position,
                    transform.position + (Vector3.right * _row.Length),
                    tileMoveSpeed * Time.deltaTime);
                yield return null;
            }

            _row.Last().transform.position = transform.position + (Vector3.right * _row.Length);
            yield return StartCoroutine(MoveRow(Vector3.left, tileMoveSpeed));
        }
    }

    private IEnumerator MoveRow(Vector3 direction, float tileMoveSpeed)
    {
        Vector3 targetPos = _row[0].transform.position + direction;

        while (Vector3.Distance(_row[0].transform.position, targetPos) > 0.05f)
        {
            for (int i = 0; i < _row.Length; i++)
            {
                _row[i].transform.position = Vector3.Lerp(_row[i].transform.position,
                    _row[i].transform.position + direction, tileMoveSpeed * Time.deltaTime);
            }
            yield return null;
        }

        for (int i = 0; i < _row.Length; i++)
        {
            Vector3 finalPos = _row[i].transform.position;
            finalPos = new Vector3(Mathf.Round(finalPos.x),
                                    Mathf.Round(finalPos.y),
                                    Mathf.Round(finalPos.z));

            _row[i].transform.position = finalPos;
        }
    }
}