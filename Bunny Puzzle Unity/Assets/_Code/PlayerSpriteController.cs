﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpriteController : MonoBehaviour
{
    [SerializeField] private Sprite _playerFrontSprite;
    [SerializeField] private Sprite _playerBackSprite;
    private SpriteRenderer _spriteRenderer;

    void Start()
    {
        _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        _spriteRenderer.sprite = _playerFrontSprite;
        Board.OnGoalReached += FlipSprite;
    }

    private void OnDisable()
    {
		Board.OnGoalReached -= FlipSprite;
    }

    private void FlipSprite()
    {
        _spriteRenderer.sprite = _playerBackSprite;
    }
}
