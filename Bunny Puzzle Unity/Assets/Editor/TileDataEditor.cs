﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(TileData))]
public class TileDataEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        TileData tile = (TileData)target;
        tile.Directions = (Directions)EditorGUILayout.EnumFlagsField("Directions", tile.Directions);
    }
}